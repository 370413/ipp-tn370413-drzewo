#ifndef TREE_HEADER
#define TREE_HEADER

#include <stdint.h>

typedef struct _Tree Tree;

Tree *t;

void init_tree();

int add_node(uint32_t number);

int rightmost_child(uint32_t number);

int delete_node(uint32_t number);

int delete_subtree(uint32_t number);

int split_node(uint32_t k, uint32_t w);

int get_tree_size();

int free_tree();

#endif
