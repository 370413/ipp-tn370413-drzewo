#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "tree.h"

#define END_OF_FILE 4
#define INVALID_COMMAND 3
#define NOT_FOUND 2
#define INVALID_PARAMETER 1
#define SUCCESS 0

#define WORD_SEPARATOR " "
#define SUCCESS_MESSAGE "OK\n"
#define ERROR_MESSAGE "ERROR\n"
#define NODES_COUNT_MESSAGE "NODES: %d\n"
#define PARENT_IS_LEAF_MESSAGE "-1\n"

struct command {
	char *command_text;
	int first_argument;
	int second_argument;
};

struct command parse_line(char *line) {
	struct command c;
	c.first_argument = 0;
	c.second_argument = 0;

	c.command_text = strtok(line, WORD_SEPARATOR);
	if (!strcmp(c.command_text, "\n")) {
		return c;
	}

	char *second_word = strtok(NULL, WORD_SEPARATOR);
	c.first_argument = atoi(second_word);

	char *third_word = strtok(NULL, WORD_SEPARATOR);
	if (third_word != NULL) {
		c.second_argument = atoi(third_word);
	}

	return c;
}

void parse_parameters(char *argv[], bool *verbose_flag){
	if (argv[1] != NULL) {
		char *param_string = (char*) malloc((strlen(argv[1]) + 1) * sizeof(char));
		strcpy(param_string, argv[1]);

		if (strcmp(param_string, "") && strcmp(param_string, "-v")) {
			printf(ERROR_MESSAGE);
			exit(INVALID_PARAMETER);
		}

		*verbose_flag = !(strcmp(param_string, "-v"));
		free(param_string);
	}
}

void print_success_message(int result) {
	if (result == SUCCESS) {
		printf(SUCCESS_MESSAGE);
		return;
	}
	printf(ERROR_MESSAGE);
}

int execute_command(struct command c) {
	int result;

	if (!strcmp(c.command_text, "ADD_NODE")) {
		result = add_node(c.first_argument);
		print_success_message(result);
	}
	else if (!strcmp(c.command_text, "RIGHTMOST_CHILD")) {
		result = rightmost_child(c.first_argument);
		printf("%d\n", result);
		result = SUCCESS;
	}
	else if (!strcmp(c.command_text,"DELETE_NODE")) {
		result = delete_node(c.first_argument);
		print_success_message(result);
	}
	else if (!strcmp(c.command_text,"DELETE_SUBTREE")) {
		result = delete_subtree(c.first_argument);
		print_success_message(result);
	}
	else if (!strcmp(c.command_text,"SPLIT_NODE")) {
		result = split_node(c.first_argument, c.second_argument);
		print_success_message(result);
	}
	else if (!strcmp(c.command_text,"\n")) {
		result = END_OF_FILE;
	}
	else {
		result = INVALID_COMMAND;
	}

	return result;
}

int main(int argc, char *argv[]) {
	bool verbose_flag = false;
	struct command c;
	int result = SUCCESS;

	init_tree();

	parse_parameters(argv, &verbose_flag);

	char *line = NULL;
	size_t len = 0;

	while (getline(&line, &len, stdin) != -1) {
		c = parse_line(line);
		result = execute_command(c);

		if (result == END_OF_FILE) {
			result = SUCCESS;
			break;
		}

		if (verbose_flag) {
			fprintf(stderr, NODES_COUNT_MESSAGE, get_tree_size());
		}

		else if (result) {
			break;
		}
	}

	free(line);
	free_tree();

	return result;
}
