#ifndef LIST_HEADER
#define LIST_HEADER

#include <stdbool.h>
#include <stdint.h>

typedef struct _Node {
	uint32_t root;
	struct _Node *children;
	struct _Node *next;
	struct _Node *previous;
} Node;


bool is_empty(Node *n);

Node *get_children(Node *n);

uint32_t get_root(Node *n);

Node *get_previous(Node *n);

Node *get_next(Node *n);

Node *get_last(Node *list);

Node *get_first(Node *list);

Node *init_node(int value);

void free_node(Node *n);

Node create_node(int value);

void connect_nodes(Node *a, Node *b);

void add_previous(Node *list, Node *n);

void add_next(Node *previous, Node *n);

void add_last(Node *list, Node *n);

void add_first(Node *list, Node *n);

void add_child(Node *parent, Node *n);

#endif
