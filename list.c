#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct _Node {
	uint32_t root;
	struct _Node *children;
	struct _Node *next;
	struct _Node *previous;
} Node;

/* In this implementation Node has a double role, both as a node in the list,
 * and a representation of a list. Regular nodes have pointers: to the next one,
 * and to the previous one (and to their children). The last node in the list
 * points to a 'list terminator' – special node that represents the list.
 * List terminators' children are always NULL; Its 'next' is the first
 * node and its 'previous' is the last node. Its root doesn't matter. */

bool is_empty(Node *n) {
	return n == n->next;
}

Node *get_children(Node *n) {
	return n->children;
}

uint32_t get_root(Node *n) {
	return n->root;
}

Node *get_previous(Node *n) {
	return n->previous;
}

Node *get_next(Node *n) {
	return n->next;
}

Node *get_last(Node *list) {
	return get_previous(list);
}

Node *get_first(Node *list) {
	return get_next(list);
}

Node *init_node(uint32_t value) {
	Node* n = malloc(sizeof(Node));
	n->root = 0;
	n->children = NULL;
	n->next = n;
	n->previous = n;
	return n;
}

void free_node(Node *n) {
	free(n);
}

Node create_node(uint32_t value) {
	Node n;
	n.root = value;
	/* Lists of children are allocated separately from regular nodes,
	 * and must be freed as such */
	n.children = init_node(value);
	n.next = NULL;
	n.previous = NULL;
	return n;
}

void connect_nodes(Node *a, Node *b) {
	a->next = b;
	b->previous = a;
}

void add_previous(Node *list, Node *n) {
	Node *previous_last = list->previous;
	connect_nodes(n, list);
	connect_nodes(previous_last, n);
}

void add_next(Node *previous, Node *n) {
	return add_previous(n, previous);
}

void add_last(Node *list, Node *n) {
	return add_previous(list, n);
}

void add_first(Node *list, Node *n) {
	return add_next(list,n);
}

void add_child(Node *parent, Node *child) {
	add_last(get_children(parent), child);
}
