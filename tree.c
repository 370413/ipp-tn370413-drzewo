#include <stdlib.h>
#include <stdio.h>

#include "list.h"

#define SUCCESS 0
#define PARENT_IS_LEAF -1

/* We know that input file < 5MB which means there cannot be more than
 * 500 thousand nodes; thus we don't need to implement a dynamic array. */
#define INITIAL_SIZE 500000

typedef struct _Tree {
	Node nodes[INITIAL_SIZE];
	uint32_t count;
	uint32_t size;
} Tree;

/* In this implementation we operate on only one global tree.
 * However, it can be easily changed to accomodate for a number of trees,
 * just by adding `Tree *t` argument to functions below.
 * But since we don't need more than one, it was deemed unnecessary. */
static Tree *t = NULL;

void init_tree() {
	t = malloc(sizeof(Tree));
	Node n = create_node(0);
	t->nodes[0] = n;
	t->count = 1;
	t->size = 1;
}

int add_node(uint32_t parent_root) {
	Node n = create_node(t->count);
	t->nodes[t->count] = n;

	Node *n_ptr = &(t->nodes[t->count]);
	add_child(&(t->nodes[parent_root]), n_ptr);

	t->count++;
	t->size++;

	return SUCCESS;
}

int rightmost_child(uint32_t parent_root) {
	Node *n = &(t->nodes[parent_root]);

	Node *children = get_children(n);
	if (is_empty(children)) {
		return PARENT_IS_LEAF;
	}

	return get_root(get_last(children));
}

int delete_node(uint32_t number) {
	Node *n = &(t->nodes[number]);

	Node *before = get_previous(n);
	Node *after = get_next(n);
	Node *children = get_children(n);

	if (is_empty(children)) {
		connect_nodes(before, after);
	}
	else {
		Node *first_child = get_first(children);
		Node *last_child = get_last(children);
		connect_nodes(before, first_child);
		connect_nodes(last_child, after);
	}

	/* children are lists, outside t.nodes array – need to be freed */
	free_node(children);
	t->size--;
	return SUCCESS;
}

int delete_subtree_aux(Node *n) {
	Node *children = get_children(n);

	if (children == NULL) {
		return SUCCESS;
	}

	Node *first_child;
	while (!is_empty(children)) {
		first_child = get_first(children);
		/* children must be a proper list for `while` condition to work */
		connect_nodes(children, get_next(first_child));
		delete_subtree_aux(first_child);
	}

	t->size--;
	free_node(children);
}

int delete_subtree(uint32_t number) {
	Node *n = &(t->nodes[number]);
	delete_subtree_aux(n);

	if (number != 0) {
		connect_nodes(get_previous(n), get_next(n));
	}

	return SUCCESS;
}

int split_node(uint32_t k, uint32_t w) {
	Node *k_n = &(t->nodes[k]);
	Node *w_n = &(t->nodes[w]);
	Node *w_next = get_next(w_n);
	Node *k_children = get_children(k_n);
	Node *last_k_child = get_last(k_children);

	add_node(k);

	Node *new_node = get_last(get_children(k_n));
	Node *new_children = get_children(new_node);

	if (w_n == last_k_child) {
		/* no children nodes next to w to take from k */
		return SUCCESS;
	}
	connect_nodes(w_n, new_node);
	connect_nodes(new_node, k_children);
	connect_nodes(last_k_child, new_children);
	connect_nodes(new_children, w_next);

	return SUCCESS;
}

int get_tree_size() {
	return t->size;
}

int free_tree() {
	delete_subtree_aux(&(t->nodes[0]));
	free(t->nodes);
}
